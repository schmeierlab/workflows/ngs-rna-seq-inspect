# Snakemake workflow: ngs-rna-seq-inspect

[![pipeline status](https://gitlab.com/schmeierlab/workflows/ngs-rna-seq-inspect/badges/v0.1.0/pipeline.svg)](https://gitlab.com/schmeierlab/workflows/ngs-rna-seq-inspect/-/commits/v0.1.0)

- AUTHOR: Sebastian Schmeier (s.schmeier@pm.me)
- DATE: 2020
- VERSION: 0.1.0

Loosely based on a workflow
[here](https://github.com/snakemake-workflows/rna-seq-star-deseq2).

**This workflow is designed to help in the decision if adapter trimming should
be performed and to identify what type of library was used for sequencing
(e.g. unstranded vs stranded). We subsample a certain amount of reads from the
original samples and perform mapping (genome) and quasimapping (transcriptome)
with raw and trimmed data to compare the uniquely mapping read numbers and
assigned fragments to the transcriptome.**

It takes as input the same files as the mapping workflow at
[https://gitlab.com/schmeierlab/workflows/ngs-mapping-star](https://gitlab.com/schmeierlab/workflows/ngs-mapping-star),
so can be easily set up beforehand.


## On this page
[[_TOC_]]


## Usage


### Step 1: Install workflow

Clone the repo.


### Step 2a: Build a STAR index

For example:

```bash
STAR --runMode genomeGenerate --genomeDir ngs-test-data/ref/genome --genomeFastaFiles ngs-test-data/ref/genome/genome.chr21.fa
```


### Step 2b: Build Salmon index

```bash
salmon index --keepDuplicates -p 12 -t ngs-test-data/ref/transcriptome/Homo_sapiens.GRCh38.cdna.21.fa -i ngs-test-data/ref/transcriptome
```


### Step 3: Configure workflow

Configure the workflow according to your needs via editing the file `config.yaml` and the sheets `samples.tsv` and `units.tsv`.


### Step 4: Execute workflow

#### Singularity-only mode (recommended)

Change the `singularity` variable in the `config.yaml` to `singularity: docker://continuumio/miniconda3`

```bash
snakemake -p --use-singularity --singularity-args "--bind /mnt" --jobs 16 --configfile config.yaml 2> run.log
```

#### Singularity + Conda  mode

Change the `singularity` variable in the `config.yaml` to `singularity: docker://continuumio/miniconda3`

```bash
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt" --jobs 12 --configfile config.yaml 2> run.log
```

#### Conda-only mode

```bash
snakemake -p --use-conda --jobs 12 --configfile config.yaml 2> run.log 
```

#### Cluster execution (NESI Mahuika)

```bash
# load Singularity module and run with singularity-only mode

# run snakemake in global env
snakemake --use-singularity -j 999 --cluster-config data/nesi/cluster-nesi-mahuika.yaml --cluster "sbatch -A {cluster.account} -p {cluster.partition} -n {cluster.ntasks} -t {cluster.time} --hint={cluster.hint} --output={cluster.output} --error={cluster.error} -c {cluster.cpus-per-task}" -p --rerun-incomplete
```

    
## Test

Test data can be downloaded and the workflow executed with the current config.

```bash
git clone https://gitlab.com/schmeierlab/workflows/ngs-test-data.git

STAR --runMode genomeGenerate --genomeDir ngs-test-data/ref/genome --genomeFastaFiles ngs-test-data/ref/genome/genome.chr21.fa --genomeSAindexNbases 11

salmon index --keepDuplicates -p 12 -t ngs-test-data/ref/transcriptome/Homo_sapiens.GRCh38.cdna.21.fa -i ngs-test-data/ref/transcriptome

snakemake -p --use-singularity --singularity-args "--bind /mnt" --jobs 12 --configfile config.yaml
```


## Inspect results

The main results of the workflow can be viewed in four files:

- `mapping_stats_salmon.tsv`

```bash
## === SALMON ===
## Raw produced more assigned fragments than trimmed in 4 / 4 samples (*).
## For library-type info, please refer to: https://salmon.readthedocs.io/en/latest/library_type.html#fraglibtype
Sample  raw-lib-type    raw-num_assigned_fragments      raw-num_frags_with_consistent_mappings  trim-lib-type    trim-num_assigned_fragments     trim-num_frags_with_consistent_mappings raw>trimmed
A1      IU      236     111     IU      189     110     *
A2      IU      637     366     IU      506     364     *
B1      IU      241     108     IU      197     108     *
B2      U       636     646     U       611     621     *
```

Here, we can inspect the Salmon inferred library type and if the raw or trimmed
reads produced more assigned fragments to the transcriptome.


- `mapping_stats_star.tsv`


```bash
## === STAR ===
## Raw produced more uniquely mapped reads than trimmed in 4 / 4 samples (*).
Sample  raw-num_reads   raw-mappedunique        raw-mappedmultiloci     trim-num_reads  trim-mappeduniquetrim-mappedmultiloci    raw>trimmed
A1      2500    1517    771     2086    1463    574     *
A2      2357    1985    225     2099    1861    200     *
B1      2500    1601    718     2103    1537    524     *
B2      2398    2094    244     2329    2046    233     *
```

Here, we can inspect if mapping with STAR produced more uniquely mapped reads
with raw or trimmed reads.

- Finally, two multiqc-html files can be viewed containing the STAR mapping results.

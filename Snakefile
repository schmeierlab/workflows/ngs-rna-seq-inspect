import pandas as pd
from snakemake.utils import validate, min_version
from os.path import join, abspath
##### set minimum snakemake version #####
min_version("5.4.0")

##### DIRS
DIR_ENVS    = abspath("envs")
DIR_SCRIPTS = abspath("scripts")
DIR_RULES   = abspath("rules")
DIR_SCHEMAS = abspath("schemas")

##### load config and sample sheets #####
if config=={}:
    print('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))

##### Result dirs
DIR_BASE       = abspath(config["basedir"])
DIR_RES        = join(DIR_BASE, "results")
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")

units = pd.read_csv(config["units"], dtype=str, sep="\t").set_index(["sample", "unit"], drop=False)
units.index = units.index.set_levels([i.astype(str) for i in units.index.levels])  # enforce str in index
validate(units, schema=join(DIR_SCHEMAS, "units.schema.yaml"))

##### target rules #####

TARGETS = [join(DIR_RES, "mapping_stats_star.tsv"),
           join(DIR_RES, "mapping_stats_salmon.tsv"),
           join(DIR_RES, "04_multiqc_raw/multiqc_report_raw.html"),
           join(DIR_RES, "04_multiqc_trimmed/multiqc_report_trimmed.html")]

rule all:
    input:
        TARGETS
        

##### setup singularity #####

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: config["singularity"]


##### load rules #####
include: join(DIR_RULES, "common.smk")
#include: join(DIR_RULES, "qc.smk")
include: join(DIR_RULES, "subsample.smk")
if config["trim"] == "bbduk":
    include: join(DIR_RULES, "trim_bbduk.smk")
else:
    include: join(DIR_RULES, "trim_fastp.smk")
include: join(DIR_RULES, "align.smk")
include: join(DIR_RULES, "quasimapping.smk")
include: join(DIR_RULES, "qc.smk")


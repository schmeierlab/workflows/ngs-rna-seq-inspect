import json

def xtract(file):
    d = json.loads(open(file, "r").read())
    form=d["expected_format"]
    num_ass = str(d["num_assigned_fragments"])
    if "num_frags_with_concordant_consistent_mappings" in d:
        num_conc = str(d["num_frags_with_concordant_consistent_mappings"])
    else:
        num_conc = str(d["num_frags_with_consistent_mappings"])
    return [form, num_ass, num_conc]

out = open(snakemake.output[0], "w")

num = len(snakemake.input.files_raw)

iRaw = 0
res  = []
for i in range(num):
    f_raw = snakemake.input.files_raw[i]
    f_trim = snakemake.input.files_trimmed[i]
    sample = snakemake.params.samples[i]
    res_raw = xtract(f_raw)
    res_trim = xtract(f_trim)
    ind = ""
    if int(res_raw[1]) > int(res_trim[1]):
        iRaw += 1
        ind = "*"
    res.append([sample,
                   res_raw[0],
                   res_raw[1],
                   res_raw[2],
                   res_trim[0],
                   res_trim[1],
                   res_trim[2],
                   ind])

out.write(f"## === SALMON ===\n## Raw produced more assigned fragments than trimmed in {iRaw} / {num} samples (*).\n")
out.write("## For library-type info, please refer to: https://salmon.readthedocs.io/en/latest/library_type.html#fraglibtype\n")
out.write("Sample\traw-lib-type\traw-num_assigned_fragments\traw-num_frags_with_consistent_mappings\ttrim-lib-type\ttrim-num_assigned_fragments\ttrim-num_frags_with_consistent_mappings\traw>trimmed\n")
for a in res:
    out.write("{}\n".format("\t".join(a)))


out.close()

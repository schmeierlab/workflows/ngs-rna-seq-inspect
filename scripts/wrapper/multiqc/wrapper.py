"""Snakemake wrapper for multiqc."""
from os import path
from snakemake.shell import shell

input_dirs = set(path.dirname(fp) for fp in snakemake.input)
input_dirs2 = set(path.dirname(fp) for fp in snakemake.params.get("additional_dirs", ""))
output_dir = path.dirname(snakemake.output[0])
output_name = path.basename(snakemake.output[0])
log = snakemake.log_fmt_shell(stdout=True, stderr=True)

shell(
    "multiqc"
    " {snakemake.params.extra}"
    " --force"
    " -o {output_dir}"
    " -n {output_name}"
    " {input_dirs}"
    " {input_dirs2}"
    " {log}")

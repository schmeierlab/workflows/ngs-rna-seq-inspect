"""Snakemake wrapper for Salmon Quant
Adjusted from https://bitbucket.org/snakemake/snakemake-wrappers/raw/b48c47ecd268be992730c7abe0682e091eb1427c/bio/salmon/quant/wrapper.py
"""

__author__ = "Sebastian Schmeier"
__copyright__ = "Copyright 2018, Sebastian Schmeier"
__email__ = "s.schmeier@gmail.com"
__license__ = "MIT"

from os import path
from snakemake.shell import shell

extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=False, stderr=True)
libtype = snakemake.params.get("libtype", "A")

sample = snakemake.input.get("sample")
fq1 = sample[0]
assert fq1 is not None, "input-> fq1 is a required input parameter"

if len(sample) == 2: ## paired end
    fq2 = sample[1]
    read_cmd = f" -1 {fq1} -2 {fq2}"
else: ## single end
    read_cmd = f" -r {fq1}"
    
outdir = path.dirname(snakemake.output.get('quant'))

shell("salmon quant -i {snakemake.params.index} "
      " -l {libtype} {read_cmd} -o {outdir} "
      " -p {snakemake.threads} {extra} {log} ")

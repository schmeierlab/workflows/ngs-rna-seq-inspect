import csv

def xtract(file):
    reader = csv.reader(open(file, "r"), delimiter="|")
    res = []
    for a in reader:
        a = [s.strip() for s in a]
        if len(a) != 2:
            continue
        if a[0] == "Number of input reads":
            res.append(a[1])
        elif a[0] == "Uniquely mapped reads number":
            res.append(a[1])
        elif a[0] == "Number of reads mapped to multiple loci":
            res.append(a[1])
    assert len(res) == 3
    return res

out = open(snakemake.output[0], "w")

num = len(snakemake.input.files_raw)

iRaw = 0
res  = []
for i in range(num):
    f_raw = snakemake.input.files_raw[i]
    f_trim = snakemake.input.files_trimmed[i]
    sample = snakemake.params.samples[i]
    res_raw = xtract(f_raw)
    res_trim = xtract(f_trim)
    ind = ""
    if int(res_raw[1]) > int(res_trim[1]):
        iRaw += 1
        ind = "*"
    res.append([sample,
                   res_raw[0],
                   res_raw[1],
                   res_raw[2],
                   res_trim[0],
                   res_trim[1],
                   res_trim[2],
                ind])

out.write(f"## === STAR ===\n## Raw produced more uniquely mapped reads than trimmed in {iRaw} / {num} samples (*).\n")
out.write("Sample\traw-num_reads\traw-mappedunique\traw-mappedmultiloci\ttrim-num_reads\ttrim-mappedunique\ttrim-mappedmultiloci\traw>trimmed\n")
for a in res:
    out.write("{}\n".format("\t".join(a)))


out.close()

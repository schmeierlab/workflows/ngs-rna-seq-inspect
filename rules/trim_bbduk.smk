# bbduk adapter and quality trimming
rule bbduk_pe:
    input:
        get_subsampled
    output:
        out1=join(DIR_RES, "01_trimmed/{sample}-{unit}.1.fastq.gz"),
        out2=join(DIR_RES, "01_trimmed/{sample}-{unit}.2.fastq.gz")
    log:
        join(DIR_LOGS, "bbduk/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "bbduk/{sample}-{unit}.txt")
    priority: 20
    threads: 2
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    params:
        extra=config["extra"]["bbduk"],
        adapters=abspath(config["adapters"]),
        stats=join(DIR_LOGS, "bbduk/{sample}-{unit}.stats")  # can be parsed by multiqc
    shell:
        "bbduk.sh {params.extra} "
        " threads={threads}"
        " ref={params.adapters}"
        " stats={params.stats}"
        " in1={input[0]}"
        " in2={input[1]}"
        " out1={output.out1}"
        " out2={output.out2}"
        " > {log} 2>&1"


rule bbduk_se:
    input:
        get_subsampled
    output:
        out=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastq.gz")
    log:
        join(DIR_LOGS, "bbduk/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "bbduk/{sample}-{unit}.txt")
    priority: 20
    threads: 2
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    params:
        extra=config["extra"]["bbduk"],
        adapters=abspath(config["adapters"]),
        stats=join(DIR_LOGS, "bbduk/{sample}-{unit}.stats")  # can be parsed by multiqc
    shell:
        "bbduk.sh {params.extra}"
        " threads={threads}"
        " ref={params.adapters}"
        " stats={params.stats}"
        " in={input[0]}"
        " out={output.out}"
        " > {log} 2>&1"
    


def is_single_end(sample, unit):
    return pd.isnull(units.loc[(sample, unit), "fq2"])


def get_trimmed(wildcards):
    if not is_single_end(**wildcards):
        # paired-end sample
        return expand(join(DIR_RES, "01_trimmed/{sample}-{unit}.{group}.fastq.gz"),
                      group=[1, 2], **wildcards)
    # single end sample
    return [join(DIR_RES, "01_trimmed/{sample}-{unit}.fastq.gz".format(**wildcards))]


def get_subsampled(wildcards):
    if not is_single_end(**wildcards):
        # paired-end sample
        return expand(join(DIR_RES, "00_sampled/{sample}-{unit}.{group}.fastq.gz"),
                      group=[1, 2], **wildcards)
    # single end sample
    return [join(DIR_RES, "00_sampled/{sample}-{unit}.fastq.gz".format(**wildcards))]

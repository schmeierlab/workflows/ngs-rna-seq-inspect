def get_trimmed_reports(wildcards):
    if config["trim"] == "bbduk":
        return expand(join(DIR_LOGS, "bbduk/{unit.sample}-{unit.unit}.stats"), unit=units.itertuples())
    else:
        return expand(join(DIR_RES, "01_trimmed/{unit.sample}-{unit.unit}.fastp.json"), unit=units.itertuples())

        
rule multiqc_raw:
    input:
        # star
        expand(join(DIR_RES, "02_star_raw/{unit.sample}-{unit.unit}/Log.final.out"), unit=units.itertuples())
    output:
        join(DIR_RES, "04_multiqc_raw/multiqc_report_raw.html")
    log:
        join(DIR_LOGS, "multiqc_raw.log")
    benchmark:
        join(DIR_BENCHMARKS, "multiqc_raw.txt")
    conda:
        join(DIR_ENVS, "multiqc.yaml")
    params:
        extra="",
        additional_dirs=""
    wrapper:
        "file://scripts/wrapper/multiqc"


rule multiqc_trimmed:
    input:
        # star
        expand(join(DIR_RES, "02_star_trimmed/{unit.sample}-{unit.unit}/Log.final.out"), unit=units.itertuples())
    output:
        join(DIR_RES, "04_multiqc_trimmed/multiqc_report_trimmed.html")
    log:
        join(DIR_LOGS, "multiqc_trimmed.log")
    benchmark:
        join(DIR_BENCHMARKS, "multiqc_trimmed.txt")
    conda:
        join(DIR_ENVS, "multiqc.yaml")
    params:
        extra="",
        additional_dirs=get_trimmed_reports
    wrapper:
        "file://scripts/wrapper/multiqc"

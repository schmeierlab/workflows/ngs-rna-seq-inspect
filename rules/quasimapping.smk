rule salmon_raw:
    input:
        # here I read the fq1 (and fq2) from samplesheet. wrapper takes care
        # to decide if pe or se run should be done or if files need to be decompressed
        sample=get_subsampled
    output:
        quant=join(DIR_RES,'03_salmon_raw/{sample}-{unit}/quant.sf'),
        lib=join(DIR_RES,'03_salmon_raw/{sample}-{unit}/lib_format_counts.json')
    log:
        join(DIR_LOGS, "salmon_raw/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "salmon_raw/{sample}-{unit}.txt")
    params:
        libtype="A",
        index=config["salmon"]["index"],
        extra=config["extra"]["salmon"]
    threads: 4
    conda:
        join(DIR_ENVS, "salmon.yaml")
    wrapper:
        "file://scripts/wrapper/salmon_quant"


rule salmon_trimmed:
    input:
        # here I read the fq1 (and fq2) from samplesheet. wrapper takes care
        # to decide if pe or se run should be done or if files need to be decompressed
        sample=get_trimmed
    output:
        quant=join(DIR_RES,'03_salmon_trimmed/{sample}-{unit}/quant.sf'),
        lib=join(DIR_RES,'03_salmon_trimmed/{sample}-{unit}/lib_format_counts.json')
    log:
        join(DIR_LOGS, "salmon_trimmed/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "salmon_trimmed/{sample}-{unit}.txt")
    params:
        libtype="A",
        index=config["salmon"]["index"],
        extra=config["extra"]["salmon"]
    threads: 4
    conda:
        join(DIR_ENVS, "salmon.yaml")
    wrapper:
        "file://scripts/wrapper/salmon_quant"

                
rule get_stats_salmon:
    input:
        # by using units.itertuples we make sure that the list is in a
        # certain order, so that we can associate column names to each
        files_trimmed=expand(join(DIR_RES,'03_salmon_trimmed/{unit.sample}-{unit.unit}/lib_format_counts.json'),
               unit=units.itertuples()),
        files_raw=expand(join(DIR_RES,'03_salmon_raw/{unit.sample}-{unit.unit}/lib_format_counts.json'),
               unit=units.itertuples())
    output:
        join(DIR_RES, "mapping_stats_salmon.tsv")
    log:
        join(DIR_LOGS, "get_stats_salmon.log")
    params:
        samples=units["sample"].tolist()
    script:
        join(DIR_SCRIPTS, "stats_salmon.py")

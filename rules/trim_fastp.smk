rule fastp_pe:
    input:
        sample=get_subsampled
    output:
        trimmed=[join(DIR_RES, "01_trimmed/{sample}-{unit}.1.fastq.gz"),
                 join(DIR_RES, "01_trimmed/{sample}-{unit}.2.fastq.gz")],
        html=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastp.html"),
        json=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastp.json")
    log:
        join(DIR_LOGS, "fastp_pe/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "fastp_pe/{sample}-{unit}.txt")
    threads: 2
    conda:
        join(DIR_ENVS, "fastp.yaml")
    params:
        extra=config["extra"]["fastp"]
    wrapper:
        "file://scripts/wrapper/fastp"


rule fastp_se:
    input:
        sample=get_subsampled
    output:
        trimmed=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastq.gz"),
        html=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastp.html"),
        json=join(DIR_RES, "01_trimmed/{sample}-{unit}.fastp.json")
    log:
        join(DIR_LOGS, "fastp_se/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "fastp_se/{sample}-{unit}.txt")
    threads: 2
    conda:
        join(DIR_ENVS, "fastp.yaml")
    params:
        extra=config["extra"]["fastp"]
    wrapper:
        "file://scripts/wrapper/fastp"


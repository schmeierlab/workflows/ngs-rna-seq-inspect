rule star_mapping_trimmed:
    input:
        sample=get_trimmed
    output:
        # see STAR manual for additional output files
        temp(join(DIR_RES, "02_star_trimmed/{sample}-{unit}/Aligned.out.bam")),
        join(DIR_RES, "02_star_trimmed/{sample}-{unit}/Log.final.out")
    log:
        join(DIR_LOGS, "star_mapping_trimmed/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "star_mapping_trimmed/{sample}-{unit}.txt")
    priority: 10
    conda:
        join(DIR_ENVS, "star.yaml")
    params:
        # path to STAR reference genome index
        index=config["star"]["index"]
    threads: 16
    wrapper:
        "file://scripts/wrapper/star"

        
rule star_mapping_raw:
    input:
        sample=get_subsampled
    output:
        # see STAR manual for additional output files\
        temp(join(DIR_RES, "02_star_raw/{sample}-{unit}/Aligned.out.bam")),
        join(DIR_RES, "02_star_raw/{sample}-{unit}/Log.final.out")
    log:
        join(DIR_LOGS, "star_mapping_raw/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "star_mapping_raw/{sample}-{unit}.txt")
    priority: 10
    conda:
        join(DIR_ENVS, "star.yaml")
    params:
        # path to STAR reference genome index
        index=config["star"]["index"]
    threads: 16
    wrapper:
        "file://scripts/wrapper/star"

        
rule get_stats:
    input:
        # by using units.itertuples we make sure that the list is in a
        # certain order, so that we can associate column names to each
        files_trimmed=expand(join(DIR_RES, "02_star_trimmed/{unit.sample}-{unit.unit}/Log.final.out"),
               unit=units.itertuples()),
        files_raw=expand(join(DIR_RES, "02_star_raw/{unit.sample}-{unit.unit}/Log.final.out"),
               unit=units.itertuples())
    output:
        join(DIR_RES, "mapping_stats_star.tsv")
    params:
        samples=units["sample"].tolist()
    script:
        join(DIR_SCRIPTS, "stats.py")

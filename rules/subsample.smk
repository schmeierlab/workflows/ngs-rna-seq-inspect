def get_fastq(wildcards):
    return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()


rule subsample_pe:
    input:
        get_fastq
    output:
        out1=join(DIR_RES, "00_sampled/{sample}-{unit}.1.fastq.gz"),
        out2=join(DIR_RES, "00_sampled/{sample}-{unit}.2.fastq.gz")
    benchmark:
        join(DIR_BENCHMARKS, "subsample_pe/{sample}-{unit}.txt")
    log:
        join(DIR_LOGS, "subsample_pe/{sample}-{unit}.txt")
    conda:
        join(DIR_ENVS, "seqtk.yaml")
    params:
        num=config["num_reads"]
    shell:
        "seqtk sample -s100 {input[0]} {params.num} 2> {log} | gzip > {output.out1}; "
        "seqtk sample -s100 {input[1]} {params.num} 2>> {log} | gzip > {output.out2}"


rule subsample_se:
    input:
        get_fastq
    output:
        join(DIR_RES, "00_sampled/{sample}-{unit}.fastq.gz")
    benchmark:
        join(DIR_BENCHMARKS, "subsample_se/{sample}-{unit}.txt")
    conda:
        join(DIR_ENVS, "seqtk.yaml")
    params:
        num=config["num_reads"]
    shell:
        "seqtk sample -s100 {input} {params.num} | gzip > {output}"


